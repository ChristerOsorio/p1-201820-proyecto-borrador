package model.logic;

import API.IManager;
import model.data_structures.Cola;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Lista;
import model.data_structures.Merge;

import model.vo.Bike;
import model.vo.Bike2;
import model.vo.Bike3;
import model.vo.Station;
import model.vo.Trip;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1

	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2

	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3

	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4

	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2

	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4

	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";



	private Lista <Trip> listaTrips;
	private Lista <Station> listaEstaciones;
	private Lista <String> archivosTripCargados= new Lista <String>();
	private Lista <String> archivosStationCargados= new Lista <String>();


	public ICola<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal)  {
		
		Cola<Trip> colaViajesEntre=new Cola<Trip>();
		
		for(Trip v:listaTrips)
		{
					
		if(pFechaInicial.isBefore(v.getStartTime())&&pFechaFinal.isAfter(v.getStopTime()))
		{
		colaViajesEntre.enqueue(v);
		}
		
		}
		
		return colaViajesEntre;
		}


	public ILista<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
	
		Lista<Bike> listaBicis=new Lista<Bike>();
		ICola<Trip> cola=A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal);
		ArrayList<Bike> arrTem= new ArrayList<>(); 
		Bike[] arr1= new Bike[cola.getSize()];
		int count = 0; 
		for(int i=0;i<arr1.length;i++){
			Trip elem=cola.dequeue();
			if (i == 0) { 
				
				Bike bici=new Bike(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
			arrTem.add(bici); 
				} 
			
			if (i > 0) { 
				for (int t = 0; t < arrTem.size(); t++) { 
				if (elem.getBikeId() == arrTem.get(t).getBikeId()) { 
				count++; 
				arrTem.get(t).sumarViaje(1);
				arrTem.get(t).sumarRec(calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())));
				} 
				} 
				if (count == 0) { 
					Bike bici=new Bike(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
				arrTem.add(bici); 
				} 
				} 
				count = 0; 
			
			
			
		
		}
		Bike[]arr2=arrTem.toArray(new Bike[arrTem.size()]);
		Merge.sort(arr2);
		for(Bike v:arr2)
		{
			listaBicis.addAtEnd(v);
		}
		
		return listaBicis;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public ILista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista <Trip> viajes= new Lista<Trip>();

		for(Trip v:A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal))
		{
			
			if(v.getBikeId()==bikeId  )
			{
				viajes.addAtEnd(v);
			}
		}

		return viajes;
	}

	public ILista<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista <Trip> viajes= new Lista<Trip>();

		for(Trip v:A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal))
		{
			
			if(v.getEndStationId()==endStationId )
			{
				viajes.addAtEnd(v);
			}
		}

		return viajes;
	}

	public ICola<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {
		Cola <Station> colaEstacionesFechaInicio = new Cola <Station>();
		
		for (Station v :listaEstaciones)
		{
			if(v.getStartDate().compareTo(fechaComienzo)>1)
			{
				colaEstacionesFechaInicio.enqueue(v);
			}
		}

		return colaEstacionesFechaInicio;
	}

	public ILista<Bike3> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista<Bike3> listaBicis=new Lista<Bike3>();
		ICola<Trip> cola=A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal);
		ArrayList<Bike3> arrTem= new ArrayList<>(); 
		Bike3[] arr1= new Bike3[cola.getSize()];
		int count = 0; 
		for(int i=0;i<arr1.length;i++){
			Trip elem=cola.dequeue();
			if (i == 0) { 
				
				Bike3 bici=new Bike3(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
			arrTem.add(bici); 
				} 
			
			if (i > 0) { 
				for (int t = 0; t < arrTem.size(); t++) { 
				if (elem.getBikeId() == arrTem.get(t).getBikeId()) { 
				count++; 
				arrTem.get(t).sumarViaje(1);
				arrTem.get(t).sumarRec(calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())));
				} 
				} 
				if (count == 0) { 
					Bike3 bici=new Bike3(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
				arrTem.add(bici); 
				} 
				} 
				count = 0; 
			
			
			
		
		}
		Bike3[]arr2=arrTem.toArray(new Bike3[arrTem.size()]);
		Merge.sort(arr2);
		for(Bike3 v:arr2)
		{
			listaBicis.addAtEnd(v);
		}
		
		return listaBicis;
	}

	public ILista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
		Lista <Trip> viajesDuracion= new Lista<Trip>();

		for(Trip v:listaTrips)
		{
			
			if(v.getBikeId()==bikeId && v.getTripDuration()<= tiempoMaximo && v.getGender().equals(genero))
			{
				viajesDuracion.addAtEnd(v);
			}
		}

		return viajesDuracion;
	}

	public ILista<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista <Trip> viajesEstacionInicial= new Lista<Trip>();
		

		for(Trip v:A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal))
		{
			
			if(v.getStartStationId()==startStationId )
			{
				viajesEstacionInicial.addAtEnd(v);
			}
		}

		

		return viajesEstacionInicial;
	}

	public void C1cargar(String rutaTrips, String rutaStations) {
		
		//agregar estaciones;

		FileReader reader;
		Lista<Trip> listaViajesTemporal= new Lista<Trip>();
		Lista<Station> listaEstacionesTemporal= new Lista<Station>();
		
		

		try {
			reader= new FileReader(rutaStations);

			@SuppressWarnings("resource")
			BufferedReader lector= new BufferedReader(reader);
			lector.readLine();
			String linea= lector.readLine();


			while(linea!=null)
			{


				String[] partes= linea.split(",");
				Station  estacionAAgregar;




				estacionAAgregar= new Station(partes[0].replaceAll("\"",""), partes[1].replaceAll("\"",""), partes[2].replaceAll("\"",""), partes[3].replaceAll("\"",""),  partes [4].replaceAll("\"",""), partes [5].replaceAll("\"",""),stringToDateTime(partes [6].replaceAll("\"","")));







				listaEstacionesTemporal.addAtEnd(estacionAAgregar);
				linea= lector.readLine();


			}
			
			
			
			
		
			

		lector.close();
			
			
			
			
			
			
			//agregar viajes
			
			
			
			listaEstaciones=listaEstacionesTemporal;
			System.out.println("Estaciones:"+listaEstaciones.size());


	


		
			reader= new FileReader(rutaTrips);


			BufferedReader lector2= new BufferedReader(reader);
			lector2.readLine();
			String linea2= lector2.readLine();




			while(linea2!=null)
			{

				String[] partes= linea2.split(",");


				if (partes.length==12)
				{

					Trip  viajeAAgregar;
					
					
					




					viajeAAgregar= new Trip(Integer.parseInt(partes[0].replaceAll("\"","")), stringToDateTime(partes[1].replaceAll("\"","")), stringToDateTime(partes[2].replaceAll("\"","")), Integer.parseInt(partes[3].replaceAll("\"","")), Integer.parseInt(partes[4].replaceAll("\"","")), Integer.parseInt(partes[5].replaceAll("\"","")), Integer.parseInt(partes[7].replaceAll("\"","")), partes [10].replaceAll("\"",""));







					listaViajesTemporal.addAtEnd(viajeAAgregar);
					linea2= lector2.readLine();
				}
				
				else if (partes.length==11)
				{

					Trip  viajeAAgregar;




					viajeAAgregar= new Trip(Integer.parseInt(partes[0].replaceAll("\"","")), stringToDateTime(partes[1].replaceAll("\"","")), stringToDateTime(partes[2].replaceAll("\"","")), Integer.parseInt(partes[3].replaceAll("\"","")), Integer.parseInt(partes[4].replaceAll("\"","")), Integer.parseInt(partes[5].replaceAll("\"","")), Integer.parseInt(partes[7].replaceAll("\"","")), "NO APLICA");





					

					listaViajesTemporal.add(viajeAAgregar);
					linea2= lector2.readLine();
				}
				
			}
			
			listaTrips=listaViajesTemporal;
			System.out.println("Viajes: "+listaTrips.size());
			lector2.close();
			
		}
		catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public ICola<Trip> C2ViajesValidadosBicicleta1(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}
	
	
/*/
	
public void C1cargar(String rutaTrips, String rutaStations) {
		
		//Cargar las estaciones	

		//Si no hay estaciones cargadas, carga el archivo que le llegue
		if(listaEstaciones==null)
		{
			listaEstaciones= new Lista <Station>();
			FileReader readerStation;
			Lista<Station> listaStationsTemporal= new Lista<Station>();

			try {
				readerStation= new FileReader(rutaStations);

				@SuppressWarnings("resource")
				BufferedReader lectorStation= new BufferedReader(readerStation);
				lectorStation.readLine();
				String lineaStation= lectorStation.readLine();


				while(lineaStation!=null)
				{


					String[] partesStation= lineaStation.split(",");
					Station  estacionAAgregar;




					estacionAAgregar= new Station(partesStation[0].replaceAll("\"",""), partesStation[1].replaceAll("\"",""), partesStation[2].replaceAll("\"",""), partesStation[3].replaceAll("\"",""),  partesStation [4].replaceAll("\"",""), partesStation [5].replaceAll("\"",""),stringToDateTime(partesStation [6].replaceAll("\"","")));








					listaStationsTemporal.add(estacionAAgregar);
					lineaStation= lectorStation.readLine();	

				}


				Station[] arrayStations=	listaToStationArray(listaStationsTemporal);
				Merge.sort(arrayStations);
				for(int i=0;i<arrayStations.length;i++)
				{
					listaEstaciones.add(arrayStations[i]);
//					System.out.println(listaStations.get(i).getStationId());
				}

				archivosStationCargados.add(rutaStations);
			}

			catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

		}

		//si ya hay estaciones cargadas, solo selecciona las estaciones cuyo id no este en la lista para meterlas

		else if (listaEstaciones!=null && !rutaStations.equals(archivosStationCargados.get(0))&& !rutaStations.equals(archivosStationCargados.get(1)))
		{

			FileReader readerStation;
//			Lista<Station> listaStationsTemporal= listaStations;
			Station[] arrayStationsTemporal=	listaToStationArray(listaEstaciones);

			try {
				readerStation= new FileReader(rutaStations);

				@SuppressWarnings("resource")
				BufferedReader lectorStation= new BufferedReader(readerStation);
				lectorStation.readLine();
				String lineaStation= lectorStation.readLine();


				while(lineaStation!=null)
				{


					String[] partesStation= lineaStation.split(",");
					Station  estacionAAgregar;


					if(searchIndexOfStation(arrayStationsTemporal, Integer.parseInt(partesStation[0]))== -1)
					{
						estacionAAgregar= new Station(partesStation[0].replaceAll("\"",""), partesStation[1].replaceAll("\"",""), partesStation[2].replaceAll("\"",""), partesStation[3].replaceAll("\"",""),  partesStation [4].replaceAll("\"",""), partesStation [5].replaceAll("\"",""),stringToDateTime(partesStation [6].replaceAll("\"","")));


						listaEstaciones.add(estacionAAgregar);
						
						
					}





					lineaStation= lectorStation.readLine();	

				}


				
				Station[] arrayStations=	listaToStationArray(listaEstaciones);
				Quick.sort(arrayStations);
				listaEstaciones= new Lista<>();
				for(int i=0;i<arrayStations.length;i++)
				{
					listaEstaciones.add(arrayStations[i]);
//					System.out.println(listaStations.get(i).getStationId());
				}

				archivosStationCargados.add(rutaStations);
//				System.out.println("total estaciones: "+ listaStations.size());
				return;
			}

			catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}





		FileReader reader;
		Lista<Trip> listaTemporal= new Lista<Trip>();


		if(archivosTripCargados!=null)
		{

			for (int i=0; i< archivosTripCargados.size();i++)
			{
				if (archivosTripCargados.get(i).compareTo(rutaTrips)==0)
				{
					return;
				}
			}
		}


		try {
			reader= new FileReader(rutaTrips);


			BufferedReader lector= new BufferedReader(reader);
			lector.readLine();
			String linea= lector.readLine();




			while(linea!=null)
			{

				String[] partes= linea.split(",");

				Trip  viajeAAgregar;
				Bike bykeAgregar;
				boolean existeBicicleta=false;
				if (partes.length==12)
				{

					


					String loquesea= partes[1].replaceAll("\"","");




					viajeAAgregar= new Trip(Integer.parseInt(partes[0].replaceAll("\"","")), stringToDateTime(partes[1].replaceAll("\"","")), stringToDateTime(partes[2].replaceAll("\"","")), Integer.parseInt(partes[3].replaceAll("\"","")), Integer.parseInt(partes[4].replaceAll("\"","")), Integer.parseInt(partes[5].replaceAll("\"","")), Integer.parseInt(partes[7].replaceAll("\"","")), partes [10].replaceAll("\"",""));







					listaTemporal.add(viajeAAgregar);
					linea= lector.readLine();
				}

				
				
				
				
				else 
//					if (partes.length==11)
				{

					




					viajeAAgregar= new Trip(Integer.parseInt(partes[0].replaceAll("\"","")), stringToDateTime(partes[1].replaceAll("\"","")), stringToDateTime(partes[2].replaceAll("\"","")), Integer.parseInt(partes[3].replaceAll("\"","")), Integer.parseInt(partes[4].replaceAll("\"","")), Integer.parseInt(partes[5].replaceAll("\"","")), Integer.parseInt(partes[7].replaceAll("\"","")), "NO APLICA");







					listaTemporal.add(viajeAAgregar);
					linea= lector.readLine();
				}
				
				
				
//				







			listaTrips=listaTemporal;
			archivosTripCargados.add(rutaTrips);



		}
		}
		catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	*/
	
	public Station darEstacion(int id)
	{Station a=null;
		for(Station v:listaEstaciones)
		{
			if(id==v.getId())
			{
				a=v;
			}
		}
		return a;
	}

	public ILista<Bike2> C3BicicletasMasUsadas(int topBicicletas) {
		Lista<Bike2> listaBicis=new Lista<Bike2>();
		
		ArrayList<Bike2> arrTem= new ArrayList<>(); 
		Bike2[] arr1= new Bike2[listaTrips.size()];
		int count = 0; 
		for(Trip elem:listaTrips){
			
			if (arrTem.isEmpty()==true) { 
				
				Bike2 bici=new Bike2(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
			arrTem.add(bici); 
				} 
			
			else if (arrTem.isEmpty()==false) { 
				for (Bike2 elem2:arrTem) { 
				if (elem.getBikeId() == elem2.getBikeId()) { 
				count++; 
				elem2.sumarViaje(1);
				elem2.sumarRec(calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())));
				} 
				} 
				if (count == 0) { 
					Bike2 bici=new Bike2(elem.getBikeId(),1,calculateDistance(Double.parseDouble(darEstacion(elem.getEndStationId()).getLatitud()),Double.parseDouble(darEstacion(elem.getEndStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLongitud()),Double.parseDouble(darEstacion(elem.getStartStationId()).getLatitud())),elem.getTripDuration());
				arrTem.add(bici); 
				} 
				} 
				count = 0; 
			
			
			
		
		}
		Bike2[]arr2=arrTem.toArray(new Bike2[arrTem.size()]);
		Merge.sort(arr2);
		for(int i=0;i<topBicicletas;i++)
		{
			listaBicis.addAtEnd(arr2[i]);
		}
		
		return listaBicis;
	}

	public ILista<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		Lista <Trip> viajes= new Lista<Trip>();

		for(Trip v:A1ViajesEnPeriodoDeTiempo( fechaInicial,  fechaFinal))
		{
			
			if(v.getStartStationId()==StationId )
			{
				viajes.addAtEnd(v);
			}
			else if(v.getEndStationId()==StationId) {
				viajes.addAtEnd(v);
			}
		}

		return viajes;
	}

	//metodos auxiliares

	//calcular distancias

	public double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {
		final int R = 6371*1000; // Radius of the earth in meters 
		double latDistance = Math.toRadians(latitudReferencia-lat1); 
		double lonDistance = Math.toRadians(longitudReferencia-lon1); 
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		double distance = R * c;
		return distance; 
	}

	public LocalDateTime stringToDateTime(String pFechaString)
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM.dd.yyyy HH:mm:ss");

		String date = pFechaString.replaceAll("/",".");
		date= date.replaceAll("-",".");
		
		String[] dateParts= date.split("\\.");
		if (dateParts[0].length()==1)
		{
			dateParts[0]="0"+dateParts[0];
		}
		if (dateParts[1].length()==1)
		{
			dateParts[1]="0"+dateParts[1];
		}
		
		String fixedDate= dateParts[0]+"."+dateParts[1]+"."+dateParts[2];

		LocalDateTime localDateTime = LocalDateTime.parse(fixedDate, formatter);

		return localDateTime;
	}


	
	
	public Trip [] listaToTripArray(Lista <Trip> pLista)
	{
		Trip [] arrayTrips= new Trip[pLista.size()];
		
		for(int i=0; i<pLista.size(); i++)
		{
			arrayTrips[i]=pLista.get(i);
		}
		
		return arrayTrips;
	}
	public Station [] listaToStationArray(Lista <Station> pLista)
	{
		Station [] arrayStation= new Station[pLista.size()];
//		int  posicion=0;
int i=0;
		for(Station v:pLista)
		{
			arrayStation[i]=v;
			i++;
		}
		
//		Iterator <Station> iteradorListaStation= listaStations.iterator();
//		while(iteradorListaStation.hasNext())
//		{
//			arrayStation[posicion]=iteradorListaStation.next();
//			posicion ++;
//		}
				

		return arrayStation;
	}
	public static int searchIndexOfStation(Station[] a,int key) {
		int lo = 0;
		int hi = a.length - 1;
		while (lo <= hi) {
			// Key is in a[lo..hi] or not present.
			int mid = lo + (hi - lo) / 2;
			if    (key<a[mid].getId())  hi = mid - 1;
			else if (key>a[mid].getId()) lo = mid + 1;
			else return mid;
		}
		return -1;
	}


	@Override
	public ICola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		// TODO Auto-generated method stub
		return null;
	}



}
