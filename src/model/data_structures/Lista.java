package model.data_structures;

import java.util.Iterator;

/*
 * Basada en mi ejercicio de nivel 9 de apo 2, el libro de introduccion a las estructuras de datos de Villalobos, y 
 * de la implementacion de Pablo Barvo para Cupi2 
 * (recuperado de:https://cupintranet.virtual.uniandes.edu.co/sitio/index.php/cursos/estructuras-de-datos/cupi2collections
 * /estructuras-de-datos/lista-encadenada/visualizacion-codigo-fuente/listaencadenada)
 */


public class Lista <T extends Comparable <T>> implements Iterable<T>, ILista <T>{

	//Atributos
	
	private Node <T> primero;
	private Node <T>ultimo;
	private int numeroElementosLista;

	//Constructor
	public Lista() {
		primero = null;
		ultimo = null;
		numeroElementosLista=0;
	}


	//Metodos



	@Override
	public int size() {

		return numeroElementosLista;
	}

	@Override
	public void add(T pItem) {
		// 
		Node <T> nodoAgregar= new Node<T>(pItem);

		if (primero==null)
		{
			primero= nodoAgregar;
			ultimo=nodoAgregar;

		}

		else
		{

			primero.setAnterior(nodoAgregar);
			primero=nodoAgregar;
		}

		numeroElementosLista++;
	}


	public void addAtEnd(T pItem) {
		Node <T> nodoAgregar= new Node<T>(pItem);

		if (primero==null)
		{
			primero= nodoAgregar;
			ultimo=nodoAgregar;

		}

		else
		{

			ultimo.setSiguiente(nodoAgregar);
			ultimo=nodoAgregar;
		}

		numeroElementosLista++;
	}

	
	public void addAtK(T pItem, int pPosicion) throws Exception {


		if (pPosicion>=numeroElementosLista)
		{
			throw new Exception("posición no valida en la lista");
		}

		Node<T> nodoAgregar= new Node <T>(pItem);

		Node<T> nodoActual= primero;

		for(int i=0; i<pPosicion-1; i++)
		{
			nodoActual=nodoActual.getSiguiente();
		}

		nodoActual.setSiguiente(nodoAgregar);
		numeroElementosLista++;



	}

	@Override
	public T get(int pPosicion) {
		if (pPosicion>=numeroElementosLista)
		{
			return null;
		}

		

		Node<T> nodoActual= primero;

		for(int i=0; i<pPosicion; i++)
		{
			nodoActual=nodoActual.getSiguiente();
		}

		return nodoActual.getItem();
	}

//	@Override
//	public T getCurrentElement() {
//		return primero.darItem();
//	}
//
//	@Override
//	public void delete() {
//		// TODO Auto-generated method stub
//
//	}

	
	
	public void deleteAtK(int pPosicion) throws Exception  {
		
		if(  pPosicion >= numeroElementosLista )
		{
		throw new Exception();
		}
		else if( pPosicion == 0 )
		{
		if( primero.equals( ultimo ) )
		{
		ultimo = null;
		}
		
		primero = primero.getSiguiente();
		numeroElementosLista--;
		
		}
		else
		{

		Node<T> p = primero.getSiguiente( );
		for( int cont = 1; cont < pPosicion; cont++ )
		{
		p = p.getSiguiente( );
		}

		if( p.equals( ultimo ) )
		{
		ultimo = p.getAnterior( );
		}
		
		p.setItem(null);
		numeroElementosLista--;
		}
	

	}

	@Override
	public Iterator<T> iterator() {
		return new IteradorParaListaDobleEnlazada();
	}

	class IteradorParaListaDobleEnlazada implements Iterator <T>{

		Node<T> nodoActual=null;
		@Override
		public boolean hasNext()
		{
			if (nodoActual==null && primero !=null)
			{
				return true;
			}
			else if(nodoActual!=null)
			{
				return nodoActual.getSiguiente()!=null;
			}
			return false;
		}

		@Override
		public T next()
		{
			if (nodoActual==null)
			{
				nodoActual=primero;
			}

			else 
				{nodoActual= nodoActual.getSiguiente();}

			return nodoActual.getItem();
		}
	}

	@Override
	public T remove(T elem) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public T get(T elem) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean isEmpty() {
		
		if(size()==0)
		{
			return true;
		}
		return false;
	}

}
