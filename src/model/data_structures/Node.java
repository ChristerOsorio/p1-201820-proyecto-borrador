package model.data_structures;

public class Node <E> {
	
	public Node<E> siguiente;
	
	public Node<E> anterior;
	
	private E item;
	
	
	public Node (E n) {
	siguiente = null;
	anterior=null;
	this.item = n;}
	
	public Node<E> getSiguiente() {
	return siguiente;}
	
	public void setSiguiente ( Node<E> next) {
	this.siguiente = next;}
	
	public void setAnterior ( Node<E> ant) {
		this.siguiente = ant;}
	
	public Node<E> getAnterior ( ) {
		return anterior;}
	
	
		
		
	
	public E getItem(){
	return item;}
	
	public void setItem(E e){
		item=e;
		}
	
	

}
