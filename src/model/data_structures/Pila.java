package model.data_structures;

import java.util.Iterator;



public class Pila<E> implements IPila<E>{
	
	private Node<E> top = null;
    private int size;
   
    
 public Pila()
 {
	 top= null;
	 size=0;
 }

	

	public E element() throws Exception {
        if (top == null) {
            throw new Exception("Queue does not contain any items.");
        }

        return top.getItem();
    }

	public boolean isEmpty() {
		return top == null;
	}

	public int getSize() {
		
		return size;
	}

	public void push(E t) {
		Node<E> newItem = new Node<E>(t);

        if (top == null) {
            top = newItem;
            size++;
        } else {
            // New Top
            newItem.setSiguiente(top);
            top = newItem;
            size++;
        }

        return;
		
	}

	public E pop()  {
		
		if (top == null) {
            return null;
        }

        E out = top.getItem();
        top=top.getSiguiente();
        size--;

        return out;
		
	}



	public Iterator<E> iterator() {
		return new IteradorParaListaDobleEnlazada();
	}

	class IteradorParaListaDobleEnlazada implements Iterator <E>{

		Node<E> nodoActual=null;
		@Override
		public boolean hasNext()
		{
			if (nodoActual==null && top !=null)
			{
				return true;
			}
			else if(nodoActual!=null)
			{
				return nodoActual.getSiguiente()!=null;
			}
			return false;
		}

		@Override
		public E next()
		{
			if (nodoActual==null)
			{
				nodoActual=top;
			}

			else 
				{nodoActual= nodoActual.getSiguiente();}

			return nodoActual.getItem();
		}
	}

	public Node<E> getTop() {
		// TODO Auto-generated method stub
		return top;
	}



	
	

}
