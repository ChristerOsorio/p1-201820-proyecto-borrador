package model.data_structures;

import java.util.Iterator;


public class Cola<E> implements ICola<E>, Iterable<E> {

	private Node<E> first=null;
	private Node<E> last=null;
	private int siz=0;

	public Iterator<E> iterator() {
		return new IteradorParaListaDobleEnlazada();
	}

	class IteradorParaListaDobleEnlazada implements Iterator <E>{

		Node<E> nodoActual=null;
		@Override
		public boolean hasNext()
		{
			if (nodoActual==null && first !=null)
			{
				return true;
			}
			else if(nodoActual!=null)
			{
				return nodoActual.getSiguiente()!=null;
			}
			return false;
		}

		@Override
		public E next()
		{
			if (nodoActual==null)
			{
				nodoActual=first;
			}

			else 
			{nodoActual= nodoActual.getSiguiente();}

			return nodoActual.getItem();
		}
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return  first==null;
	}

	public int getSize() {
		// TODO Auto-generated method stub

		return siz;
	}
	public E element() throws Exception {
		if (first == null) {
			throw new Exception("Queue does not contain any items.");
		}

		return first.getItem();
	}
	@Override

	public void enqueue(E t) {
		// TODO Auto-generated method stub
		Node<E> newElement = new Node<E>(t);
        if (first == null) {
            first = newElement;
            last= newElement;
        } else {
            Node <E> oldLast=last;
            oldLast.setSiguiente(newElement); 
            last= newElement;
        }
siz++;


	}
	@Override
	public E dequeue()  {
		if (first == null) {
			return null;
		}

		E output = first.getItem();
        this.first=this.first.getSiguiente();
        siz--;
        return output;
	}

}
