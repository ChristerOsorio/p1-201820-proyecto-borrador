package model.vo;

import java.util.Iterator;

public class Bike2 implements Comparable<Bike2>{

    private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public Bike2(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike2 o) {
       	if (o.getTotalDuration()>this.totalDuration){
    			return 1; 
    		}
    		else if (o.getTotalDuration()<this.totalDuration){
    			return -1; 
    		}
    		
    		else
    			return 0;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }

	public void sumarViaje(int i) {
		totalTrips=totalTrips+i;
		
	}

	public void sumarRec(double d) {
		totalDistance =totalDistance +d;
		
	}

	
}
