package model.vo;

import java.util.Iterator;

public class Bike3 implements Comparable<Bike3>{

    private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public Bike3(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    public int compareTo(Bike3 o) {
       	if (o.getTotalDistance()>this.totalDistance){
    			return 1; 
    		}
    		else if (o.getTotalDistance()<this.totalDistance){
    			return -1; 
    		}
    		
    		else
    			return 0;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }

	public void sumarViaje(int i) {
		totalTrips=totalTrips+i;
		
	}

	public void sumarRec(double d) {
		totalDistance =totalDistance +d;
		
	}

	
}
