package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int id;
	private String nombre;
	private String ciudad;
	private String latitud;
	private String longitud;
	private int dpCapacity;
	private LocalDateTime startDate;
	//TODO Completar

	public Station(String pId, String pNombre, String pCiudad, String pLatitud, String pLongitud, String pdpCapacity, LocalDateTime pStartDate) {
		setId(Integer.parseInt(pId));
		setNombre(pNombre);
		setCiudad(pCiudad);
		setLatitud(pLatitud);
		setLongitud(pLongitud);
		setDpCapacity(Integer.parseInt(pdpCapacity));
		setStartDate(pStartDate);
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		if (this.id>o.getId())
		{
			return 1;
		}
		else if (this.id<o.getId())
		{
			return -1;
		}
		else
			return 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public int getDpCapacity() {
		return dpCapacity;
	}

	public void setDpCapacity(int dpCapacity) {
		this.dpCapacity = dpCapacity;
	}

	public LocalDateTime getStartDate() {
		// TODO Auto-generated method stub
		return startDate;
	}
	public void setStartDate(LocalDateTime pStartTime) {
		// TODO Auto-generated method stub
		this.startDate=pStartTime;
	}
}
